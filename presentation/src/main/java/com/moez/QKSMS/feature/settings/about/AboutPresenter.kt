/*
 * Copyright (C) 2017 Moez Bhatti <moez.bhatti@gmail.com>
 *
 * This file is part of QKSMS.
 *
 * QKSMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QKSMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QKSMS.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.moez.QKSMS.feature.settings.about

import android.net.Uri
import com.moez.QKSMS.R
import com.moez.QKSMS.common.Navigator
import com.moez.QKSMS.common.base.QkPresenter
import com.uber.autodispose.android.lifecycle.scope
import com.uber.autodispose.autoDisposable
import javax.inject.Inject

class AboutPresenter @Inject constructor(
        private val navigator: Navigator
) : QkPresenter<AboutView, Unit>(Unit) {

    override fun bindIntents(view: AboutView) {
        super.bindIntents(view)


        view.preferenceClicks()
                .autoDisposable(view.scope())
                .subscribe { preference ->
                    when (preference.id) {
                        R.id.fork -> navigator.openUri(Uri.parse("https://github.com/moezbhatti/qksms"))
                        R.id.source -> navigator.openUri(Uri.parse("https://gitlab.e.foundation/e/apps/message"))
                        R.id.copyright -> navigator.openUri(Uri.parse("https://gitlab.e.foundation/e/apps/Message/-/blob/master/AUTHORS"))
                        R.id.license -> navigator.openUri(Uri.parse("https://gitlab.e.foundation/e/apps/Message/-/blob/master/LICENSE"))
                        R.id.author -> navigator.openUri(Uri.parse("https://gitlab.e.foundation/e/apps/Message/-/blob/master/AUTHORS"))
                        R.id.privacy -> navigator.openUri(Uri.parse("https://e.foundation/legal-notice-privacy/"))
                        R.id.service_terms -> navigator.openUri(Uri.parse("https://e.foundation/legal-notice-privacy/"))
                        R.id.app_version -> navigator.openUri(Uri.parse("https://gitlab.e.foundation/e/apps/Message/-/releases"))
                    }
                }
    }

}
